<?php

class PlaylistCancion extends Eloquent  {
	
	
	protected $table = 'playlistcancion';
	protected $primaryKey = "playlistcancionid";
	
	public static function getCanciones($id){
		return DB::table('playlistcancion AS pc')
			->select('c.titulo','u.username','c.usuarioid','c.cancionid')
			->leftJoin('canciones AS c','c.cancionid','=','pc.cancionid')
			->leftJoin('usuarios AS u','u.usuarioid','=','c.usuarioid')
			->where('pc.playlistid',$id)
			->get();
	
	}
	
	
	
}
