<?php

class Comentario extends Eloquent  {
	
	
	protected $table = 'comentarios';
	protected $primaryKey = "comentarioid";
	
	
    public function user()
    {
      return $this->belongsTo('User', 'usuarioid', 'usuarioid');
    }
  
  public static function comentarios($id){
    return DB::table('comentarios AS c')
    ->select('c.text','c.second','u.username','c.usuarioid','u.avatar')
    ->leftJoin('usuarios AS u','u.usuarioid','=','c.usuarioid')
    ->where('c.cancionid',$id)
    ->orderBy('c.created_at')
    ->get();
  }
	
}
