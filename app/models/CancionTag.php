<?php

class CancionTag extends Eloquent  {
	
	
	protected $table = 'canciontag';
	protected $primaryKey = "canciontagid";
	
	public static function getTags($id){
		return DB::table('canciontag AS ct')
			->select('ct.canciontagid AS id','t.nombre')
			->leftJoin('tags AS t','t.tagid','=','ct.tagid')
			->where('ct.cancionid',$id)
			->get();
	
	}
	public static function getCanciones($tags){
		return DB::table('canciontag AS ct')
			->select('ct.cancionid')
			->leftJoin('tags AS t','t.tagid','=','ct.tagid')
			->leftJoin('canciones AS c','c.cancionid','=','ct.cancionid')
			->whereRaw('FIND_IN_SET(t.nombre,"'.$tags.'")>0')
			->get();
	}
	
	
	
}
