<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});
//canciones
Route::get('/cancion/play/{id}', array('as'=>'play','uses'=>'CancionController@play'));
Route::post('/cancion/titulo/{id}', array('as'=>'guardarTitulo','uses'=>'CancionController@guardarTitulo'));
Route::post('/cancion/tag', array('as'=>'guardarTag','uses'=>'CancionController@guardarTitulo'));
Route::post('/uploadAvatar', array('as'=>'uploadAvatar','uses'=>'UsuarioController@uploadAvatar'));
Route::post('/uploadCover/{id}', array('as'=>'uploadCover','uses'=>'CancionController@uploadCover'));
Route::post('/agregarTag/{id}', array('as'=>'agregarTag','uses'=>'TagController@agregarTag'));
Route::post('/deleteTag/{id}', array('as'=>'deleteTag','uses'=>'TagController@deleteTag'));
Route::post('/aPlaylist/{id}', array('as'=>'aPlaylist','uses'=>'PlaylistController@aPlaylist'));
Route::post('/favorito/{id}', array('as'=>'favorito','uses'=>'UsuarioController@favorito'));
Route::post('/follow/{id}', array('as'=>'follow','uses'=>'UsuarioController@follow'));
Route::post('/unfollow/{id}', array('as'=>'unfollow','uses'=>'UsuarioController@unfollow'));
Route::post('/favorito/quitar/{id}', array('as'=>'quitarFavorito','uses'=>'UsuarioController@quitarFavorito'));
Route::post('/playlist/borrar/{id}', array('as'=>'borrarPlaylist','uses'=>'PlaylistController@borrar'));




Route::get('/perfil', array('as'=>'perfil','uses'=>'UsuarioController@perfil'));
Route::get('/perfil/{id}', array('uses'=>'UsuarioController@perfil'));
Route::resource('usuario', 'UsuarioController'); 
Route::resource('cancion', 'CancionController'); 
Route::resource('comentario', 'ComentarioController'); 
Route::resource('playlist', 'PlaylistController'); 
Route::post('/login', array('as' => 'login', 'uses' => 'UsuarioController@login'));
Route::post('/logout', array('as' => 'logout', 'uses' => 'UsuarioController@logout'));

Route::get('/misplaylists', array('as' => 'misplaylists', 'uses' => 'PlaylistController@playlists'));
Route::get('/playlists/{id}', array('as' => 'playlist', 'uses' => 'PlaylistController@playlists'));


Route::get('/favoritos/{id}', array('as' => 'favoritos', 'uses' => 'UsuarioController@favs'));

Route::get('/playlist/songs/{id}', array('as' => 'playlist', 'uses' => 'PlaylistController@songs'));
Route::get('/perfil/canciones/{id}', array('as' => 'canciones', 'uses' => 'UsuarioController@songs'));
Route::get('/search', array('as' => 'search', 'uses' => 'CancionController@search'));
