<?php

class PlaylistController extends BaseController {
  
	public function songs($id)
	{
		$user = Playlist::find($id)->user;
		$songs = PlaylistCancion::getCanciones($id);
		$likes = array();
		$tags = array();
		foreach($songs as $song){
			$likes[$song->cancionid] = Favorito::where("cancionid","=",$song->cancionid)->count();
			$tags[$song->cancionid] = CancionTag::getTags($song->cancionid);
		}
		return View::make('playlist')
			->with("canciones",$songs)
			->with("likes",$likes)
			->with("usuario",$user)
			->with("tags",$tags);
	}
		
		
	public function playlists($id=null)
	{

		if($id==null)	
			$id = Auth::id();
		$playlists = Playlist::where("usuarioid","=",$id)->get();
		$cont = array();
		foreach($playlists as $p){
			$cont[$p->playlistid] = PlaylistCancion::where("playlistid","=",$p->playlistid)->count();
		}
		$user = User::find($id);

		return View::make('playlists')
			->with("usuario",$user)
			->with("playlists",$playlists)
			->with("cont",$cont);
	}
	
	public function store(){
		$data = Input::only(['nombre']);
			$validator = Validator::make(
				$data,
				[
						'nombre' => 'required'
				],
				[
					'nombre.required' => 'Nombre requerido'
				]
		);
		
		if($validator->fails()){
				return $validator->messages()->toJson();
		}
		$playlist = Playlist::whereRaw("nombre ='".$data['nombre']."' AND usuarioid=".Auth::id())->count();
		if($playlist>0)
			return array("message"=>"Ya existe playlist con ese nombre");
		$playlist = new Playlist;
		$playlist->nombre = $data['nombre'];
		$playlist->usuarioid = Auth::id();
		$playlist->save();
		return array("result"=>"1","id"=>$playlist->playlistid);
	
	}
	
	public function aPlaylist($id){
		$data = Input::only(['playlist']);
			$validator = Validator::make(
				$data,
				[
						'playlist' => 'required'
				],
				[
					'required' => 'Playlist requerido'
				]
		);
		
		if($validator->fails()){
				return $validator->messages()->toJson();
		}
		
		$playlist = Playlist::find((int)$data['playlist'])->count();
		if($playlist==0)
			return array("message","Playlist no existe");
		
		$playlist = new PlaylistCancion;
		$playlist->cancionid = $id;
		$playlist->playlistid = (int)$data['playlist'];
		$playlist->save();
		//return array("result"=>"1","id"=>$playlist->playlistcancionid);
		return Redirect::to('/cancion/play/'.$id);
	}
	
	public function borrar($id)
	{
		$play= Playlist::find($id);
		$play->delete();
		
	}
  
}