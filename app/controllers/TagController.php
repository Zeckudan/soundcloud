<?php

class TagController extends BaseController {
  
	public function agregarTag($id)
	{
			$data = Input::only(['tag']);
			$validator = Validator::make(
							$data,
							[
									'tag' => 'required|min:3'
							],
							[
								'required'=>'Ingrese el nombre del tag',
								'min'=>'El minimo de caracteres es 3'
							]
					);
					
		if($validator->fails()){
			return $validator->messages()->toJson();
		}
		$nombre = strtolower($data['tag']);
		$nombre = str_replace("-"," ",$nombre);
		$nombre = addslashes($nombre);
		$tag = Tag::where('nombre', '=', $nombre)->first();
		if(!$tag){
			$tag = new Tag;
			$tag->nombre = $nombre;
			$tag->save();
			
			
		}else{
			$rel = CancionTag::whereRaw("cancionid = ".(int)$id." AND tagid =".$tag->tagid)->count();
			if($rel>0)
				return array("message"=>"La cancion ya tiene este tag");
		}
		$tagid = $tag->tagid;
		$rel = new CancionTag;
		$rel->cancionid = $id;
		$rel->tagid = $tagid;
		$rel->save();
		
		return array("result"=>"1","id"=>$rel->canciontagid);
	}
	
	public function deleteTag($id)
	{
		$rel = CancionTag::find($id);
		if(!$rel)
			return array("message"=>"Peticion invalida");
		
		$rel->delete();
		
		return array();
	
	}
  
}