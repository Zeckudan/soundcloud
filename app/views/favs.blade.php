@extends('template/mainTemplate')

@section('content')

<div class="profile-desc page-header">
  <div class="header-picture">
       @if($usuario->avatar=="")
        <img src="/images/cover.jpg" id="avatar" alt="Profile Picture">
      @else
        <img src="/images/{{$usuario->avatar}}" id="avatar" alt="Profile Picture">
      @endif
  </div>
  <div class="header-info">
	<h3>{{$usuario->username}}</h3>
	<h1>{{"@".$usuario->username}}</h1>
  </div>
  <div class="usr-menu">
    <ul class="nav">
      <li><a href="#">Playlists</a></li>
      <li><a href="#">Canciones</a></li>
      <li><a class="active" href="#">Favoritos</a></li>
      <li><a href="#">Siguiendo</a></li>
    </ul>
  </div>
</div>

<div class="feed songs">
  @foreach($favs as $fav)
		<div class="post">
			<div class="pull-right">
				<div class="likes">
					<a href=""><i class="fa fa-smile-o"></i></a>
					<span>{{$cont[$fav->cancionid]}}</span>
				</div>
				<div class="delete-btn">
					<button>
						<i class="fa fa-minus-circle"></i>
						Quitar de favoritos
					</button>
				</div>
			</div>
			<h4>{{$fav->titulo}}</h4>
			<a href="/cancion/play/{{$fav->cancionid}}"><button class="play"><i class="fa fa-play"></i></button></a>
			<div class="post-info">
				<p>{{$fav->username}}</p>
				<p>
					@foreach($tags[$fav->cancionid] as $tag)
						<a href="/search?busqueda={{$tag->nombre}}" class="tag">{{$tag->nombre}}</a>
					@endforeach
				</p>
		</div>
		</div>
	@endforeach
	
   <div class="post">
    <div class="pull-right">
      <div class="likes">
        <a href=""><i class="fa fa-smile-o"></i></a>
        <span>31</span>
      </div>
      <div class="delete-btn">
        <button>
          <i class="fa fa-minus-circle"></i>
          Quitar de favoritos
        </button>
      </div>
    </div>
    <h4>Nombre de la canción</h4>
    <button class="play"><i class="fa fa-play"></i></button>
    <div class="post-info">
      <p>Artista</p>
      <p><a href="#" class="tag">tag1</a><a href="#" class="tag">tag2</a><a href="#" class="tag">tag3</a></p>
	</div>
  </div>
  <div class="post">
    <div class="pull-right">
      <div class="likes">
        <a class="liked" href=""><i class="fa fa-smile-o"></i></a>
        <span>427</span>
      </div>
      <div class="delete-btn">
        <button>
          <i class="fa fa-minus-circle"></i>
          Quitar de favoritos
        </button>
      </div>
    </div>
    <h4>Last Train to London</h4>
    <button class="play"><i class="fa fa-play"></i></button>
    <div class="post-info">
      <p>Electric Light Orchestra</p>
      <p><a href="#" class="tag">Disco music</a><a href="#" class="tag">Dance</a></p>
	</div>
  </div>
</div>

@stop