@extends('template/mainTemplate')

@section('stylesheets')
{{ HTML::style('css/flashblock.css'); }}
{{ HTML::style('css/360player-visualization.css'); }}
{{ HTML::style('css/360player.css'); }}
@stop


@section('content')

<div class="song-desc page-header">
	<div class="header-picture">
		<img src="/images/cover.jpg" alt="cover"/>
	</div>
	<div class="header-info">
		<h2>{{$cancion->titulo}}</h2>
		<p>
		  @foreach($tags as $tag)
            <a href="/search?busqueda={{$tag->nombre}}" id="tag{{$tag->id}}" class="tag">{{$tag->nombre}}<button rel="{{$tag->id}}" class="deleteTag">X</button></a>
          @endforeach
		</p>
	</div>
	<div class="song-user">
		<div>
			<h5>uploaded by</h5>
			<h3 class="username"><a href="#">{{"@".$usuario->username}}</a></h3>
		</div>
		<img src="/images/cover.jpg" alt="cover"/>
	</div>
</div>

<div class="player-container">
   @if(Auth::check())
  <ul class="nav">
     @if($fav)
      <li>{{ Form::submit('Agregar a favoritos', array('class' => 'add-btn hide','id'=>'agregarFavorito')) }}</li>
      <li>{{ Form::submit('Quitar de favoritos', array('class' => 'add-btn','id'=>'quitarFavorito')) }}</li>
    @else
      <li>{{ Form::submit('Agregar a favoritos', array('class' => 'add-btn','id'=>'agregarFavorito')) }}</li>
      <li>{{ Form::submit('Quitar de favoritos', array('class' => 'add-btn hide','id'=>'quitarFavorito')) }}</li>
    @endif
    <li>{{ Form::submit('Agregar a playlist', array('class' => 'add-btn','id'=>'agregarPlaylist')) }}</li>
  </ul>
  @endif
  <div class="player">
	<div class="ui360 ui360-vis"><a href="/audio/{{$cancion->filepath}}"></a></div>
	<div id="sm2-container">
		<!-- sm2 flash goes here -->
	</div>
	<div id="player-comment" class="player-comment">
        <div class="arrow"></div><!--
     --><div class="popover-container">
            <h3 class="popover-title">Popover right</h3>
            <div class="popover-content">
                <p>Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
            </div>
        </div>
	</div>
  </div>
</div>

<div class="comment-section feed">

   @if(Auth::check() && Auth::id()!=$cancion->usuarioid)
     {{ Form::open(array('route' => array('comentario.store'),'class'=>'post', 'id'=>'formComentario','method' => 'post')) }}
      {{Form::hidden('id',Crypt::encrypt($cancion->cancionid))}}
      {{ Form::text('comentario', null,array('class' => 'comment-field','placeholder'=>'Escribe tu comentario')) }}
      <div class="time-input">
        <label>Minuto:</label>
        {{ Form::text('minuto', '0:00',array('class' => 'comment-time')) }}
      </div>
    {{ Form::close() }}
	@endif
	
	@foreach($comentarios as $comentario)
	<div class="comment post">
		<div class="comment-avatar">
		    @if($comentario->avatar=="")
			  <img src="/images/cover.jpg" alt="cover"/>
			@else
			  <img src="/images/{{$comentario->avatar}}" alt="cover"/>
		    @endif
		</div>
		<div class="comment-text">
			<h4 class="comment-user">
              {{"@".$comentario->username}} at {{$comentario->second}}
			</h4>
          {{$comentario->text}}
		</div>
	</div>

	@endforeach	
	
</div>

 @if(Auth::check())
   <div class="modal playlist-modal" hidden>
    <button class="close-btn">
        <i class="fa fa-times"></i>
    </button>
    <div class="container">
        <h2>Seleccionar Playlist</h2>
        {{ Form::open(array('route' => array('aPlaylist',$cancion->cancionid), 'method' => 'post','class' => 'dl')) }}
          <dl>
            <dt>Elija el playlist</dt>
            <dd>
             {{ Form::select('playlist', $playlists) }}
            </dd>
            <dt></dt>
            <dd>
              {{Form::submit('Agregar', array('class' => 'register-btn','id'=>'aPlaylist'))}}
            </dd>
          </dl>
       {{ Form::close() }}
       <hr>
        <h2>Crear Playlist</h2>
        {{ Form::open(array('route' => array('playlist.store'), 'id'=>'formPlaylist','method' => 'post','class' => 'dl')) }}
          <dl>
            <dt>Nombre</dt>
            <dd>
             {{ Form::text('nombre', null,array('placeholder' => 'Ingrese Nombre')) }}
            </dd>
            <dt></dt>
            <dd>
              {{Form::submit('Crear', array('class' => 'register-btn','id'=>'crearPlaylist'))}}
            </dd>
          </dl>
       {{ Form::close() }}

      </div>
    </div>
  @endif
@stop

@section('scripts')
{{ HTML::script('js/bootstrap-tooltip.js'); }}
{{ HTML::script('js/bootstrap-popover.js'); }}
{{ HTML::script('js/soundmanager2.js'); }}
{{ HTML::script('js/berniecode-animator.js'); }}
{{ HTML::script('js/360player.js'); }}
{{ HTML::script('js/player.js'); }}

<script type="text/javascript">
$(document).ready(function(){
   $(".comment-field").keypress(function(event){
     if(event.keyCode == 13){
      $.post("/comentario",$("#formComentario").serialize(),function(data){
        var message = "";
        $.each(data, function(i, val) {
          message+=val+"\n";
        });
        if(message.length>0)
          alert(message);
        else{
          $("#formComentario").after('<div class="comment post">'
            +'<div class="comment-avatar">'
            +'	<img src="/images/cover.jpg" alt="cover"/>'
            +'</div>'
            +'<div class="comment-text">'
            +'	<h4 class="comment-user">'
            +'		{{"@".(Auth::check()?Auth::user()->username:"")}} at '+$(".comment-time").val()
            +'	</h4>'
            + $(".comment-field").val()
            +'</div>'
            +'</div>');
          $(".comment-field").val("");
        }
      },'json');
     }
  });
  
  $("#agregarPlaylist").click(function(){
    
      jQuery(".fondo-modal").fadeIn();
      jQuery(".playlist-modal").slideDown();  

      
    });
@if(Auth::check())
  $("#agregarFavorito").click(function(){
    $.post("/favorito/{{$cancion->cancionid}}",function(data){
      $("#agregarFavorito").toggleClass("hide");
      $("#quitarFavorito").toggleClass("hide");
    });  
  });  
  
  $("#quitarFavorito").click(function(){
    $.post("/favorito/quitar/{{$cancion->cancionid}}",function(data){
      $("#agregarFavorito").toggleClass("hide");
      $("#quitarFavorito").toggleClass("hide");
    });
  });
 @endif  
  $("#crearPlaylist").click(function(e){
    e.preventDefault();
    $.post("/playlist",$("#formPlaylist").serialize(),function(data){
      if(data.result=="1"){
        $.post("/aPlaylist/{{$cancion->cancionid}}",{'playlist': data.id },function(d){
          if(d.result=="1")
            window.location.href = "/misplaylists";
          else{
           var message = "";
            $.each(d, function(i, val) {
              message+=val+"\n";
            });
            if(message.length>0)
              alert(message); 
          }
        },'json')
      }else{
        var message = "";
          $.each(data, function(i, val) {
            message+=val+"\n";
          });
          if(message.length>0)
            alert(message);
      }

    },'json');
  });
       
});
</script>
@stop
