@extends('template/mainTemplate')

@section('content')
{{ HTML::style('css/flashblock.css'); }}
{{ HTML::style('css/360player-visualization.css'); }}
{{ HTML::style('css/360player.css'); }}

<div class="song-desc page-header">
	<div class="header-picture">
	  @if($cancion->cover=="")
		<img src="/images/cover.jpg" id="cover" alt="cover"/>
      @else
		<img src="/images/{{$cancion->cover}}" id="cover" alt="cover"/>
      @endif
	</div>
	<div class="header-info">
         
          <span >
            <h2 id="editarTitulo">
               <span id="tituloCancion">
                  {{$cancion->titulo}}
                 <button type="button" class="editButton editarTitulo" value="Editar"><i class="fa fa-pencil"></i></button>
               </span>
                {{ Form::open(array('route' => array('guardarTitulo'),'class'=>'hide','id'=>'formTitulo', 'method' => 'post')) }}
                  {{Form::text('titulo',$cancion->titulo,array('id'=>'txTitulo'))}}
                  <input type="button" class="editButton guardarTitulo" value="Guardar">
                {{ Form::close() }}
            </h2> 
          </span>
        <p>
          @foreach($tags as $tag)
              <a href="/search?busqueda={{$tag->nombre}}" id="tag{{$tag->id}}" class="tag">{{$tag->nombre}}<button rel="{{$tag->id}}" class="deleteTag">X</button></a>
          @endforeach
              <input type="button" class="editButton agregarTag" value="Agregar tags">
                {{ Form::open(array('route' => array('guardarTag'),'class'=>'hide','id'=>'formTag', 'method' => 'post')) }}
                  {{Form::text('tag')}}
                  <input type="button" class="editButton guardarTag" value="Guardar">
                {{ Form::close() }}
        </p>
	</div>
	
</div>

<div class="player-container">
  <ul class="nav">
     @if($fav)
      <li>{{ Form::submit('Agregar a favoritos', array('class' => 'add-btn hide','id'=>'agregarFavorito')) }}</li>
      <li>{{ Form::submit('Quitar de favoritos', array('class' => 'add-btn','id'=>'quitarFavorito')) }}</li>
    @else
      <li>{{ Form::submit('Agregar a favoritos', array('class' => 'add-btn','id'=>'agregarFavorito')) }}</li>
      <li>{{ Form::submit('Quitar de favoritos', array('class' => 'add-btn hide','id'=>'quitarFavorito')) }}</li>
    @endif
    <li>{{ Form::submit('Agregar a playlist', array('class' => 'add-btn','id'=>'agregarPlaylist')) }}</li>
  </ul>
  <div class="player">
	<div class="ui360 ui360-vis"><a href="/audio/{{$cancion->filepath}}"></a></div>
	<div id="sm2-container">
		<!-- sm2 flash goes here -->
	</div>
	<div id="player-comment" class="player-comment">
        <div class="arrow"></div><!--
     --><div class="popover-container">
            <h3 class="popover-title">Popover right</h3>
            <div class="popover-content">
                <p>Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
            </div>
        </div>
	</div>
  </div>
</div>

<div class="comment-section feed">

	
     @if(!Auth::check() || Auth::id()!=$cancion->usuarioid)
     {{ Form::open(array('route' => array('comentario.store'),'class'=>'post', 'method' => 'post')) }}
      {{Form::hidden('id',$cancion->cancionid)}}
      {{ Form::text('comentario', null,array('class' => 'comment-field','placeholder'=>'Escribe tu comentario')) }}
      <div class="time-input">
        <label>Minuto:</label>
        {{ Form::text('minuto', '0:00',array('class' => 'comment-time')) }}
      </div>
    {{ Form::close() }}
	@endif
	@foreach($comentarios as $comentario)
	<div class="comment post">
		<div class="comment-avatar">
		    @if($comentario->avatar=="")
			  <img src="/images/cover.jpg" alt="cover"/>
			@else
			  <img src="/images/{{$comentario->avatar}}" alt="cover"/>
		    @endif
		</div>
		<div class="comment-text">
			<h4 class="comment-user">
              <a href="/perfil/{{$comentario->usuarioid}}">{{"@".$comentario->username}}</a> at {{$comentario->second}}
			</h4>
          {{$comentario->text}}
		</div>
	</div>
	@endforeach	
</div>

<hr>

<div class="modal cover-modal" hidden>
  <button class="close-btn">
      <i class="fa fa-times"></i>
  </button>
  <div class="container">
      <h2>Cambiar cover</h2>
      {{ Form::open(array('route' => array('uploadCover',$cancion->cancionid), 'method' => 'post','class' => 'dl','files' => true)) }}
        <dl>
          <dt>Imagen</dt>
          <dd>
           {{ Form::file('imagen', ['class' => 'subirImagen']) }}
          </dd>
          <dt></dt>
          <dd>
            {{Form::submit('Subir', array('class' => 'register-btn'))}}
          </dd>
        </dl>
     {{ Form::close() }}
  </div>
</div>

<div class="modal playlist-modal" hidden>
    <button class="close-btn">
        <i class="fa fa-times"></i>
    </button>
    
        <h2 class="modal-title">Seleccionar Playlist</h2>
    <div class="container">
        {{ Form::open(array('route' => array('aPlaylist',$cancion->cancionid), 'method' => 'post','class' => 'dl')) }}
          <dl>
            <dt>Elija el playlist</dt>
            <dd>
             {{ Form::select('playlist', $playlists) }}
            </dd>
            <dt></dt>
            <dd>
              {{Form::submit('Agregar', array('class' => 'register-btn','id'=>'aPlaylist'))}}
            </dd>
          </dl>
       {{ Form::close() }}
       <hr>
        <h2>Crear Playlist</h2>
        {{ Form::open(array('route' => array('playlist.store'), 'id'=>'formPlaylist','method' => 'post','class' => 'dl')) }}
          <dl>
            <dt>Nombre</dt>
            <dd>
             {{ Form::text('nombre', null,array('placeholder' => 'Ingrese Nombre')) }}
            </dd>
            <dt></dt>
            <dd>
              {{Form::submit('Crear', array('class' => 'register-btn','id'=>'crearPlaylist'))}}
            </dd>
          </dl>
       {{ Form::close() }}

      </div>
    </div>
@stop

@section('scripts')
{{ HTML::script('js/bootstrap-tooltip.js'); }}
{{ HTML::script('js/bootstrap-popover.js'); }}
{{ HTML::script('js/soundmanager2.js'); }}
{{ HTML::script('js/berniecode-animator.js'); }}
{{ HTML::script('js/360player.js'); }}
{{ HTML::script('js/player.js'); }}

<script type="text/javascript">
$(document).ready(function(){
    
    $(".editarTitulo").click(function(){
      $("#tituloCancion").toggleClass("hide");
      $("#formTitulo").toggleClass("hide");
    });    
    $(".agregarTag").click(function(){
      $("#formTag").toggleClass("hide");
      $(".agregarTag").toggleClass("hide");
    });    
  
  $("#agregarPlaylist").click(function(){
    
    jQuery(".fondo-modal").fadeIn();
    jQuery(".playlist-modal").slideDown();  

  });
  
  $("#agregarFavorito").click(function(){
    $.post("/favorito/{{$cancion->cancionid}}",function(data){
      $("#agregarFavorito").toggleClass("hide");
      $("#quitarFavorito").toggleClass("hide");
    });  
  });  
  
  $("#quitarFavorito").click(function(){
    $.post("/favorito/quitar/{{$cancion->cancionid}}",function(data){
      $("#agregarFavorito").toggleClass("hide");
      $("#quitarFavorito").toggleClass("hide");
    });
  });
  
    $(".deleteTag").click(function(){
      var rel = $(this).attr("rel");
      $.post("/deleteTag/"+rel,function(data){
        
          var message = "";
          $.each(data, function(i, val) {
            message+=val+"\n";
          });
          if(message.length>0)
            alert(message);
          else{
            $("#tag"+rel).remove();
          }
            
        
      },'json');
    });
    $(".guardarTitulo").click(function(){
      $.post("/cancion/titulo/{{$cancion->cancionid}}",$("#formTitulo").serialize(),function(data){
        
          var message = "";
          $.each(data, function(i, val) {
            message+=val+"\n";
          });
          if(message.length>0)
            alert(message);
          else{
            $("#tituloCancion").html($('#txTitulo').val()+'<button type="button" class="editButton editarTitulo" value="Editar"><i class="fa fa-pencil"></i></button>');
            $("#tituloCancion").toggleClass("hide");
            $("#formTitulo").toggleClass("hide");
          }
            
        
      },'json');
      
    });
  
  $(".guardarTag").click(function(){
      $.post("/agregarTag/{{$cancion->cancionid}}",$("#formTag").serialize(),function(data){
          
          if(data.result=="1"){
            $(".agregarTag").before('<a href="#" id="tag'+data.id+'" class="tag">'+$("#formTag input[name=tag]").val()+'<button rel="'+data.id+'" class="deleteTag">X</button></a>');
            $("#formTag").toggleClass("hide");
            $(".agregarTag").toggleClass("hide");
            $("#formTag input[name=tag]").val("");
          }
          else{
            var message = "";
            $.each(data, function(i, val) {
              message+=val+"\n";
            });
            if(message.length>0)
              alert(message);
          }
            
        
      },'json');
    
  });
  
    $("#cover").click(function(){
        jQuery(".fondo-modal").fadeIn();
        jQuery(".cover-modal").slideDown();      
    });
  
     
  $("#crearPlaylist").click(function(e){
    e.preventDefault();
    $.post("/playlist",$("#formPlaylist").serialize(),function(data){
      if(data.result=="1"){
        $.post("/aPlaylist/{{$cancion->cancionid}}",{'playlist': data.id },function(d){
          if(d.result=="1")
            window.location.href = "/misplaylists";
          else{
           var message = "";
            $.each(d, function(i, val) {
              message+=val+"\n";
            });
            if(message.length>0)
              alert(message); 
          }
        },'json')
      }else{
        var message = "";
          $.each(data, function(i, val) {
            message+=val+"\n";
          });
          if(message.length>0)
            alert(message);
      }

    },'json');
  });
       
});
</script>
@stop




