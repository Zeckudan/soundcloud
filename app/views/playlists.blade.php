@extends('template/mainTemplate')

@section('content')

<div class="profile-desc page-header">
		@if(Auth::check() && Auth::id()!=$usuario->usuarioid)
			<button class="follow"><i class="fa fa-rocket"></i> Seguir</button>
		@elseif(Auth::check() && Auth::id()==$usuario->usuarioid)
			<button class="create"><i class="fa fa-rocket"></i> Crear playlist</button>
		@endif
  <div class="header-picture">
       @if($usuario->avatar=="")
        <img src="/images/cover.jpg" id="avatar" alt="Profile Picture">
      @else
        <img src="/images/{{$usuario->avatar}}" id="avatar" alt="Profile Picture">
      @endif
  </div>
  <div class="header-info">
	<h3>{{$usuario->username}}</h3>
	<h1>{{"@".$usuario->username}}</h1>
  </div>
  <div class="usr-menu">
    <ul class="nav">
      <li><a class="active" href="#">Playlists</a></li>
      <li><a href="/perfil/canciones/{{$usuario->usuarioid}}">Canciones</a></li>
      <li><a href="/favoritos/{{$usuario->usuarioid}}">Favoritos</a></li>
      <li><a href="#">Siguiendo</a></li>
    </ul>
  </div>
</div>

<div class="feed playlists">
  @foreach($playlists as $pl)
		<div class="post" id="pl{{$pl->playlistid}}">
		 <div class="pull-right">
				<div class="delete-btn"  id="{{$pl->playlistid}}">
					<button>
						<i class="fa fa-trash"></i>
						Borrar
					</button>
				</div>
			 <div class="songs-count">
				 <span class="number">{{$cont[$pl->playlistid]}}</span>
				 <span>canciones</span>
			 </div>
		 </div>
			<h4>{{$pl->nombre}}</h4>
			<a href="/playlist/songs/{{$pl->playlistid}}"><button class="play"><i class="fa fa-play-circle-o"></i></button></a>
		</div>
	@endforeach
	

</div>

<div class="modal playlist-modal" hidden>
    <button class="close-btn">
        <i class="fa fa-times"></i>
    </button>
    <div class="container">
        <h2>Crear Playlist</h2>
        {{ Form::open(array('route' => array('playlist.store'), 'id'=>'formPlaylist','method' => 'post','class' => 'dl')) }}
          <dl>
            <dt>Nombre</dt>
            <dd>
             {{ Form::text('nombre', null,array('placeholder' => 'Ingrese Nombre')) }}
            </dd>
            <dt></dt>
            <dd>
              {{Form::submit('Crear', array('class' => 'register-btn','id'=>'crearPlaylist'))}}
            </dd>
          </dl>
       {{ Form::close() }}

      </div>
    </div>

@stop

@section('scripts')

<script type="text/javascript">
$(document).ready(function(){
  $(".follow").click(function(){
    $.post("/follow/{{$usuario->usuarioid}}",function(data){
      $(".follow").toggleClass("hide");
    });
  });
	$(".create").click(function(){
    
    jQuery(".fondo-modal").fadeIn();
    jQuery(".playlist-modal").slideDown();  

  });
	  $("#crearPlaylist").click(function(e){
    e.preventDefault();
    $.post("/playlist",$("#formPlaylist").serialize(),function(data){
      if(data.result=="1"){
				location.reload();
      }else{
        var message = "";
          $.each(data, function(i, val) {
            message+=val+"\n";
          });
          if(message.length>0)
            alert(message);
      }

    },'json');
  });
	
	$(".delete-btn").click(function(){
		var id = $(this).attr("id");
    $.post("/playlist/borrar/"+id,function(data){
      $("#pl"+id).remove();
      
    });
		
	});
  
});
</script>
@stop