@extends('template/mainTemplate')

@section('content')

<div class="profile-desc page-header">
    
	<div class="header-picture">
      @if(Auth::user()->avatar=="")
        <img src="/images/cover.jpg" id="avatar" alt="Profile Picture">
      @else
        <img src="/images/{{Auth::user()->avatar}}" id="avatar" alt="Profile Picture">
      @endif
	</div>
	<div class="header-info">
	<h3>{{Auth::user()->username}}</h3>
	<h1>{{"@".Auth::user()->username}}</h1>
	</div>
	<div class="usr-menu">
	<ul class="nav">
	  <li><a href="/misplaylists">Playlists</a></li>
	  <li><a href="#">Canciones</a></li>
	  <li><a href="#">Favoritos</a></li>
	  <li><a href="#">Siguiendo</a></li>
	</ul>
	</div>
</div>

<div class="feed">
  <div class="post">
    <div class="pull-right">
      <div class="likes">
        <a href=""><i class="fa fa-smile-o"></i></a>
        <span>31</span>
      </div>
    </div>
    <h4>Nombre de la canción</h4>
    <button class="play"><i class="fa fa-play"></i></button>
    <div class="post-info">
      <p>@username ha agregado una nueva canción</p>
      <p><a href="#" class="tag">tag1</a><a href="#" class="tag">tag2</a><a href="#" class="tag">tag3</a></p>
    </div>
  </div>
  <div class="post">
    <div class="pull-right">
      <div class="likes">
        <a href="" class="liked"><i class="fa fa-smile-o"></i></a>
        <span>12</span>
      </div>
    </div>
    <h4>Nombre de la playlist</h4>
    <button class="play"><i class="fa fa-play-circle-o"></i></button>
    <div class="post-info">
      <p>@username ha agregado una nueva playlist</p>
      <p><a href="#" class="tag">sólo este tag</a></p>
    </div>
  </div>
  <div class="post">
    <div class="pull-right">
      <div class="likes">
        <a href=""><i class="fa fa-smile-o"></i></a>
        <span>2</span>
      </div>
    </div>
    <h4>@other_user</h4>
    <div class="post-info">
      <p>@username ahora sigue a @othe_user</p>
    </div>
  </div>
</div>



<div class="modal avatar-modal" hidden>
  <button class="close-btn">
      <i class="fa fa-times"></i>
  </button>
  <div class="container">
      <h2>Cambiar avatar</h2>
      {{ Form::open(array('route' => array('uploadAvatar'), 'method' => 'post','class' => 'dl','files' => true)) }}
        <dl>
          <dt>Imagen</dt>
          <dd>
           {{ Form::file('imagen', ['class' => 'subirImagen']) }}
          </dd>
          <dt></dt>
          <dd>
            {{Form::submit('Subir', array('class' => 'register-btn'))}}
          </dd>
        </dl>
     {{ Form::close() }}
  </div>
</div>
@stop


@section('scripts')

<script type="text/javascript">
  $(document).ready(function(){

    $("#avatar").click(function(){
        jQuery(".fondo-modal").fadeIn();
        jQuery(".avatar-modal").slideDown();      
    });
  });
</script>
@stop