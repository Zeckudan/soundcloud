@extends('template/mainTemplate')

@section('content')

<div class="playlist-desc page-header">
	<div class="header-picture">
		<i class="fa fa-3x fa-play-circle-o"></i>
	</div>
	<div class="header-info">
		<h2>Nombre de la playlist</h2>
    </div>
	@if($usuario->usuarioid!=Auth::id())
	<div class="song-user">
		<div>
			<h5>created by</h5>
			<h3 class="username"><a href="/perfil/{{$usuario->usuarioid}}">{{"@".$usuario->username}}</a></h3>
		</div>
      @if($usuario->avatar=="")
        <img src="/images/cover.jpg" id="avatar" alt="Profile Picture">
      @else
        <img src="/images/{{$usuario->avatar}}" id="avatar" alt="Profile Picture">
      @endif
	</div>
	@endif
</div>

<div class="feed songs">
	@foreach($canciones as $cancion)
		<div class="post">
			<div class="pull-right">
				<div class="likes">
					<a href=""><i class="fa fa-smile-o"></i></a>
					<span>{{$likes[$cancion->cancionid]}}</span>
				</div>
				<div class="delete-btn">
					<button>
						<i class="fa fa-minus-circle"></i>
						Quitar de playlist
					</button>
				</div>
			</div>
			<h4>{{$cancion->titulo}}</h4>
			<a href="/cancion/play/{{$cancion->cancionid}}"><button class="play"><i class="fa fa-play"></i></button></a>
			<div class="post-info">
				<p>{{$cancion->username}}</p>
				<p>
					@foreach($tags[$cancion->cancionid] as $tag)
						<a href="/search?busqueda={{$tag->nombre}}" class="tag">{{$tag->nombre}}</a>
					@endforeach
				</p>
			</div>
		</div>
	@endforeach
	
	
</div>

@stop